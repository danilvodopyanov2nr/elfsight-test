import { IcomboBoxState, comboBoxActionTypes } from '../model/types';

interface comboBoxControlProps {
  state: IcomboBoxState;
  dispatch: React.Dispatch<any>;
  e: React.KeyboardEvent<HTMLInputElement>;
}
export const comboBoxControl = (props: comboBoxControlProps) => {
  const { e, dispatch, state } = props;

  if (e.key === 'Enter') {
    dispatch({
      type: comboBoxActionTypes.changeAllState,
      payload: {
        [comboBoxActionTypes.setShowOptions]: false,
        [comboBoxActionTypes.setActiveOption]: 0,
        [comboBoxActionTypes.setUserInput]:
          state.filteredOptions[state.activeOption].label
      }
    });
  } else if (e.key === 'ArrowUp') {
    if (state.activeOption === 0) {
      dispatch({
        type: comboBoxActionTypes.setActiveOption,
        payload: state.filteredOptions.length - 1
      });
    } else {
      dispatch({
        type: comboBoxActionTypes.setActiveOption,
        payload: state.activeOption - 1
      });
    }
  } else if (e.key === 'ArrowDown') {
    if (state.activeOption + 1 >= state.filteredOptions.length) {
      dispatch({
        type: comboBoxActionTypes.setActiveOption,
        payload: 0
      });
    } else {
      dispatch({
        type: comboBoxActionTypes.setActiveOption,
        payload: state.activeOption + 1
      });
    }
  }
};

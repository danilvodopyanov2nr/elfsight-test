import React, { FC, useRef } from 'react';
import './OptionsList.css';

interface OptionsListProps<T> {
  filteredOptions: T;
  activeOption: number;
  onClick?(e: React.MouseEvent<HTMLLIElement>): void;
}
export function OptionsList<T>(props: OptionsListProps<T>) {
  const { filteredOptions, activeOption, onClick } = props;
  const selectRef = useRef<HTMLUListElement | null>(null);

  function setChange() {
    const selected = selectRef?.current?.querySelector('.option-active');
    if (selected) {
      selected?.scrollIntoView({
        behavior: 'smooth',
        block: 'nearest'
      });
    }
  }

  return (
    <ul data-testid="ul-options" className="options" ref={selectRef}>
      {Array.isArray(filteredOptions) && filteredOptions.length ? (
        filteredOptions.map((option, index) => {
          let className;
          setTimeout(() => {
            setChange();
          }, 100);

          if (index === activeOption) {
            className = 'option-active';
          }

          return (
            <li
              className={className}
              key={option['label'] + index}
              onClick={onClick}
            >
              {option['label']}
            </li>
          );
        })
      ) : (
        <div className="no-option">
          <em>No options available.</em>
        </div>
      )}
    </ul>
  );
}

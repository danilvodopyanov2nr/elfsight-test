import React, {
  ChangeEvent,
  FC,
  InputHTMLAttributes,
  useEffect,
  useReducer,
  useRef
} from 'react';
import { OptionsList } from './components/OptionsList/OptionsList';
import { comboBoxControl } from './helpers/combo-box-controls';
import { comboBoxReducer } from './model/combo-box.reducer';
import './combo-box.css';
import { IcomboBoxState, comboBoxActionTypes } from './model/types';
import { motion } from 'framer-motion';
import { useOnClickOutside } from './helpers/clickOutsideHook';

export interface autocompleteOptions {
  label: string;
}
interface ComboBoxProps
  extends Partial<Omit<InputHTMLAttributes<HTMLInputElement>, 'onChange'>> {
  onChange?(state: IcomboBoxState): void;
  options: autocompleteOptions[];
}

export const ComboBox: FC<ComboBoxProps> = (props) => {
  const {
    options,
    value,
    onChange,
    placeholder = 'Введите значение',
    ...otherProps
  } = props;

  const inputRef = useRef<HTMLInputElement | null>(null);
  const comboboxRef = useRef<HTMLDivElement | null>(null);

  const initialState: IcomboBoxState = {
    activeOption: 0,
    showOptions: false,
    filteredOptions: [],
    userInput: (value as string) || ''
  };

  useOnClickOutside(comboboxRef, () => {
    dispatch({ type: comboBoxActionTypes.setShowOptions, payload: false });
  });

  const [state, dispatch] = useReducer(comboBoxReducer, initialState);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const userInput = e.currentTarget.value;

    const filteredOptions = options.filter(
      (option) =>
        option.label.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    dispatch({
      type: comboBoxActionTypes.changeAllState,
      payload: {
        [comboBoxActionTypes.setActiveOption]: 0,
        [comboBoxActionTypes.setShowOptions]: true,
        [comboBoxActionTypes.setFilteredOptions]: filteredOptions,
        [comboBoxActionTypes.setUserInput]: userInput
      }
    });

    if (onChange) onChange(state);
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    comboBoxControl({ e, state, dispatch });
    if (onChange) onChange(state);
  };

  const handleClickList = (e: React.MouseEvent<HTMLLIElement>) => {
    dispatch({
      type: comboBoxActionTypes.changeAllState,
      payload: {
        [comboBoxActionTypes.setActiveOption]: 0,
        [comboBoxActionTypes.setShowOptions]: false,
        [comboBoxActionTypes.setFilteredOptions]: [],
        [comboBoxActionTypes.setUserInput]: e.currentTarget.innerText
      }
    });
    if (onChange) onChange(state);
  };

  const handleClear = () => {
    dispatch({
      type: comboBoxActionTypes.changeAllState,
      payload: { ...initialState, [comboBoxActionTypes.setUserInput]: '' }
    });
  };

  const handleFocusInput = () => {
    inputRef.current?.focus();
  };

  const containerVariants = {
    expanded: {
      height: '200px'
    },
    collapsed: {
      height: '60px'
    }
  };

  const containerTransition = { type: 'spring', damping: 22, stiffness: 150 };

  return (
    <motion.div
      ref={comboboxRef}
      animate={state.showOptions ? 'expanded' : 'collapsed'}
      variants={containerVariants}
      transition={containerTransition}
      className="combo-box-wrapper"
      onClick={handleFocusInput}
    >
      <div className="input-container">
        <input
          ref={inputRef}
          placeholder={placeholder}
          value={state.userInput}
          onKeyDown={handleKeyDown}
          onChange={handleChange}
          {...otherProps}
        />
        <div className="combo-box-controls">
          <button
            data-testid="clear-button"
            className="combo-box-controls__clear"
            onClick={handleClear}
          >
            ❌
          </button>
        </div>
      </div>
      {state.showOptions && <span className="line-separator"></span>}

      {state.showOptions && state.userInput ? (
        <div className="option-content">
          <OptionsList
            onClick={handleClickList}
            activeOption={state.activeOption}
            filteredOptions={state.filteredOptions}
          />
        </div>
      ) : null}
    </motion.div>
  );
};

import { autocompleteOptions } from '../combo-box';

export interface IcomboBoxState {
  activeOption: number;
  filteredOptions: autocompleteOptions[];
  showOptions: boolean;
  userInput: string;
}

export enum comboBoxActionTypes {
  setUserInput = 'userInput',
  setFilteredOptions = 'filteredOptions',
  setShowOptions = 'showOptions',
  setActiveOption = 'activeOption',
  changeAllState = 'changeAllState'
}

export interface Action {
  type: comboBoxActionTypes;
  payload?: any;
}

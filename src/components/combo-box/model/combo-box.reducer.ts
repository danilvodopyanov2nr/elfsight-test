import { Reducer } from 'react';
import { IcomboBoxState, comboBoxActionTypes, Action } from './types';

export const comboBoxReducer: Reducer<IcomboBoxState, any> = (
  state: IcomboBoxState,
  action: Action
): IcomboBoxState => {
  const { type, payload } = action;

  switch (type) {
    case comboBoxActionTypes.setUserInput: {
      return { ...state, userInput: payload };
    }
    case comboBoxActionTypes.setFilteredOptions: {
      return { ...state, filteredOptions: payload };
    }
    case comboBoxActionTypes.setShowOptions: {
      return { ...state, showOptions: payload };
    }
    case comboBoxActionTypes.setActiveOption: {
      return { ...state, activeOption: payload };
    }
    case comboBoxActionTypes.changeAllState: {
      return { ...state, ...payload };
    }

    default: {
      throw new Error(`Unsupported type: ${type}`);
    }
  }
};

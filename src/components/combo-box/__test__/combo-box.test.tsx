import React from 'react';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import { ComboBox } from '../combo-box';

const testOptions = [
  { label: 'The Shawshank Redemption' },
  { label: 'The Godfather' },
  { label: 'The Godfather: Part II' },
  { label: 'The Dark Knight' },
  { label: '12 Angry Men' },
  { label: "Schindler's List" },
  { label: 'Pulp Fiction' }
];
describe('Combobox', () => {
  test('check default value', async () => {
    render(
      <ComboBox
        value="default value"
        data-testid="combo-box-input"
        options={[]}
      />
    );
    const inputField: HTMLInputElement = await screen.findByTestId(
      `combo-box-input`
    );
    await waitFor(() => expect(inputField.value).toBe('default value'));
  });

  test('check arrow navigation', async () => {
    render(<ComboBox data-testid="combo-box-input" options={testOptions} />);
    const inputField: HTMLInputElement = await screen.findByTestId(
      `combo-box-input`
    );
    inputField.focus();

    fireEvent.change(inputField, { target: { value: 'T' } });

    fireEvent.keyDown(inputField, { key: 'ArrowDown' });

    fireEvent.keyDown(inputField, { key: 'Enter' });

    expect(inputField.value).toBe('The Godfather');
  });
  test('check autocomplete', async () => {
    render(<ComboBox data-testid="combo-box-input" options={testOptions} />);
    const inputField: HTMLInputElement = await screen.findByTestId(
      `combo-box-input`
    );
    inputField.focus();

    fireEvent.change(inputField, { target: { value: '12' } });

    fireEvent.keyDown(inputField, { key: 'Enter' });

    expect(inputField.value).toBe('12 Angry Men');
  });

  test('check clear button', async () => {
    render(
      <ComboBox
        value="default value"
        data-testid="combo-box-input"
        options={testOptions}
      />
    );
    const clearButton: HTMLButtonElement = await screen.findByTestId(
      `clear-button`
    );
    const inputField: HTMLInputElement = await screen.findByTestId(
      `combo-box-input`
    );

    expect(inputField.value).toBe('default value');

    fireEvent.click(clearButton);

    expect(inputField.value).toBe('');
  });
});
